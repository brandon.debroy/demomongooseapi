﻿using System.Diagnostics;
using System.Net;

namespace DemoMongooseApi.Models.Responses
{
    [DebuggerDisplay("Success = {Success}, StatusCode = {StatusCode}, Response = {Response}")]
    public class MongooseResponse<T> //where T : class
    {
        public HttpStatusCode StatusCode { get; set; }

        public bool Success { get; set; }

        public T Response { get; set; }

        public string? ContentType { get; set; }
    }
}
