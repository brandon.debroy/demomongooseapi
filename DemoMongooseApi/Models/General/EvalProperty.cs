﻿using System.Diagnostics;
using System.Reflection;

namespace DemoMongooseApi.Models
{
    [DebuggerDisplay("Property={Property.Name}, Name={Name}")]
    public class EvalProperty
    {
        /// <summary>
        /// T Class property
        /// </summary>
        public PropertyInfo Property { get; set; }

        /// <summary>
        /// Config row name
        /// </summary>
        public string Name { get; set; }

        public EvalProperty() { }

        public EvalProperty(PropertyInfo property, string name)
        {
            Property = property;
            Name = name;
        }
    }
}