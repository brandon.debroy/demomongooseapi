﻿namespace DemoMongooseApi.Models
{
    public class MongooseRESTOptions
    {
        public const string ConfigKey = "MongooseREST";

        public MongoosePlatform Platform { get; set; } = MongoosePlatform.OnPremise;
        public string Configuration { get; set; } = string.Empty;
        public string BaseUrl { get; set; } = string.Empty;
        public string AuthUrl { get; set; } = string.Empty;
        public string ClientId { get; set; } = string.Empty;
        public string ClientSecret { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public int Timeout { get; set; } = 60;
        public int RefreshInterval { get; set; } = 3600;

        public bool EnableTokenRefresh { get; set; }
        public bool BypassHttps { get; set; }
    }
}
