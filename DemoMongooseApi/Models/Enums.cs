﻿/// <summary>
/// IDO Action
/// </summary>
public enum IDOAction
{
    Insert = 1,
    Update = 2,
    Delete = 4
}

/// <summary>
/// IDO action response message code
/// </summary>
public enum IDOMessageCode
{
    Inserted = 200,
    Updated = 201,
    Deleted = 202
}

/// <summary>
/// Mongoose platform
/// </summary>
public enum MongoosePlatform
{
    Cloud,
    OnPremise
}

/// <summary>
/// AES Message codes
/// </summary>
public enum AESMessageCode
{
    Ok = 600,
    Fail = 300
}