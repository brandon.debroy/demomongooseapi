﻿namespace DemoMongooseApi.Models.Mongoose
{
    public class MongooseToken
    {
        public bool Success { get; set; }
        public string? Token { get; set; }
        public string? Message { get; set; }
    }
}