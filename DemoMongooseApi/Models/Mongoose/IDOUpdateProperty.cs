﻿using System.Diagnostics;

namespace DemoMongooseApi.Models.Mongoose
{
    [DebuggerDisplay("Name = {Name}")]
    public class IDOUpdateProperty
    {
        public bool IsNull { get; set; }
        public bool Modified { get; set; } = true;
        public string Name { get; set; } = string.Empty;
        public string? Value { get; set; }

        public IDOUpdateProperty()
        {
        }

        public IDOUpdateProperty(string name, string? value, bool isModified = true)
        {
            Name = name;
            Value = value;
            IsNull = string.IsNullOrEmpty(value);
            Modified = isModified;
        }
    }
}
