﻿using Newtonsoft.Json;

namespace DemoMongooseApi.Models.Mongoose
{
    public class TokenFail
    {
        /// <summary>
        /// Código de error
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// Descripción del error
        /// </summary>
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }
}