﻿using Newtonsoft.Json;

namespace DemoMongooseApi.Models.Mongoose
{
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; } = string.Empty;

        [JsonProperty("refresh_token")]
        public string? RefreshToken { get; set; }

        [JsonProperty("token_type")]
        public string? Type { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
}