﻿using System.Diagnostics;

namespace DemoMongooseApi.Models.Mongoose
{
    [DebuggerDisplay("Message = {Message}, MessageCode = {MessageCode}, Items = {Items}")]
    public class BasicResponse
    {
        public string? Message { get; set; }

        public int MessageCode { get; set; }
        
        public List<List<IDOUpdateProperty>>? Items { get; set; }
    }
}