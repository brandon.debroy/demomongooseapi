﻿using System.Net;

namespace DemoMongooseApi.Models.Mongoose
{
    public class MongooseTokenResponse
    {
        public bool Success { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public MongooseToken? Token { get; set; }
    }
}
