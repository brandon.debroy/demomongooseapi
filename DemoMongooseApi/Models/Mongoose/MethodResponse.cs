﻿using System.Collections.Generic;
using System.Diagnostics;

namespace DemoMongooseApi.Models.Mongoose
{
    [DebuggerDisplay("Message = {Message}, MessageCode = {MessageCode}, Parameters = {Parameters}, ReturnValue = {ReturnValue}")]
    public class MethodResponse
    {
        public string Message { get; set; }
        public int MessageCode { get; set; }
        public List<string> Parameters { get; set; }
        public string ReturnValue { get; set; }
    }
}