﻿using DemoMongooseApi.Attributes;

namespace DemoMongooseApi.Models.IDOs
{
    public class AreaInteresImagen : AreaInteres
    {
        [IDOProperty("IMAGEN")]
        public string Imagen { get; set; }
    }
}
