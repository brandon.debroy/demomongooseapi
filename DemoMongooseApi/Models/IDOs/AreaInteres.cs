﻿using DemoMongooseApi.Attributes;

namespace DemoMongooseApi.Models.IDOs
{
    [IDOClass("VK_IDO_REC_PORTAL_AREA_INTERES_DEMO")]
    public class AreaInteres
    {
        [IDOProperty("COD_EMPRESA")]
        public int CodEmpresa { get; set; }

        [IDOProperty("COD_AREA_INTERES")]
        public string CodAreaInteres { get; set; }


        [IDOProperty("NOMBRE")]
        public string Nombre { get; set; }
    }
}
