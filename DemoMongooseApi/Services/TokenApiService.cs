﻿using DemoMongooseApi.Models;
using DemoMongooseApi.Models.Mongoose;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Serilog;

namespace DemoMongooseApi.Services
{
    public class TokenApiService
    {
        private readonly HttpClient Client;
        private readonly ApiSessionService apiSessionService;
        private readonly MongooseRESTOptions config;

        public TokenApiService(HttpClient httpClient, IOptions<MongooseRESTOptions> options, ApiSessionService sessionService)
        {
            config = options.Value;

            Client = httpClient;

            Client.BaseAddress = new Uri(config.AuthUrl);
            Client.Timeout = TimeSpan.FromSeconds(config.Timeout);

            Client.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/json");
            Client.DefaultRequestHeaders.Add("X-Infor-MongooseSessionType", "CustomUser");

            apiSessionService = sessionService;
        }

        public async Task<MongooseTokenResponse> LoginMongoose()
        {
            string path = $"ido/token/{Uri.EscapeDataString(config.Configuration)}/{Uri.EscapeDataString(config.Username)}/{Uri.EscapeDataString(config.Password)}";

            return await GetTokenAsync(path);
        }

        private async Task<MongooseTokenResponse> GetTokenAsync(string action, Dictionary<string, string>? parameters = null)
        {
            MongooseTokenResponse output = new MongooseTokenResponse();

            try
            {
                string requestStr = string.Empty;

                if (parameters?.Count > 0)
                {
                    var lst = parameters.Select(s => $"{s.Key}={Uri.EscapeDataString(s.Value)}").ToList();
                    requestStr = string.Join("&", lst);
                }

                string uri = $"{action}{(!string.IsNullOrEmpty(requestStr) ? "?" + requestStr : "")}";

                HttpResponseMessage? responsehttp = null;

                responsehttp = await Client.GetAsync(uri);

                string responseStr = await responsehttp.Content.ReadAsStringAsync();

                output.StatusCode = responsehttp.StatusCode;

                if (responsehttp.StatusCode == System.Net.HttpStatusCode.OK
                    || responsehttp.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    output.Success = true;

                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Debug("API GET Url: {@Url}{@ActionUrl}", Client.BaseAddress.ToString(), action);

                    // Don't deserialize empty
                    if (responsehttp.StatusCode != System.Net.HttpStatusCode.NoContent)
                    {
                        using (Stream responseStream = await responsehttp.Content.ReadAsStreamAsync())
                        using (StreamReader sr = new StreamReader(responseStream))
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            output.Token = JsonSerializer.CreateDefault().Deserialize<MongooseToken>(reader);
                        }
                    }
                }
                else
                {
                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Information("Unsuccessfull API GET Url: {@Url}{@ActionUrl} HttpCode: {@HttpCode}", Client.BaseAddress.ToString(), action, responsehttp.StatusCode);
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Error(ex, "A web exception occurred calling GET API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An exception occurred calling GET API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }

            return output;
        }
    }
}
