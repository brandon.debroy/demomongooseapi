﻿using System;
using System.Threading;
using DemoMongooseApi.Models.Mongoose;

namespace DemoMongooseApi.Services
{
    public class ApiSessionService : IDisposable
    {
        private readonly ReaderWriterLockSlim _lock_obj;

        private Token? _value;
        private bool _already_disposed;

        public Token? Token
        {
            get
            {
                _lock_obj.EnterReadLock();
                try
                {
                    return _value;
                }
                finally
                {
                    _lock_obj.ExitReadLock();
                }
            }
        }

        public ApiSessionService()
        {
            _lock_obj = new ReaderWriterLockSlim();
            _value = null;
        }

        public void SetToken(Token value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            _lock_obj.EnterWriteLock();

            try
            {
                _value = value;
            }
            finally
            {
                _lock_obj.ExitWriteLock();
            }
        }

        public void SetAuthToken(string token, int expiresIn)
        {
            _lock_obj.EnterWriteLock();

            try
            {
                _value.AccessToken = token;
                _value.ExpiresIn = expiresIn;
            }
            finally
            {
                _lock_obj.ExitWriteLock();
            }
        }

        public void SetRefreshToken(string token, int expiresIn)
        {
            _lock_obj.EnterWriteLock();

            try
            {
                _value.AccessToken = token;
                _value.ExpiresIn = expiresIn;
            }
            finally
            {
                _lock_obj.ExitWriteLock();
            }
        }

        protected virtual void Dispose(bool disposeManagedObjects)
        {
            if (!_already_disposed)
            {
                if (disposeManagedObjects)
                {
                    _lock_obj.Dispose();
                }
                _already_disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposeManagedObjects: true);
            GC.SuppressFinalize(this);
        }
    }
}
