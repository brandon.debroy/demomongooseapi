﻿using DemoMongooseApi.Helpers;
using DemoMongooseApi.Models;
using DemoMongooseApi.Models.Mongoose;
using DemoMongooseApi.Models.Responses;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Serilog;
using System.Linq.Expressions;

namespace DemoMongooseApi.Services
{
    public class MongooseApiService
    {
        private readonly HttpClient Client;
        private readonly MongooseRESTOptions config;

        public MongooseApiService(HttpClient httpClient, IOptions<MongooseRESTOptions> options, ApiSessionService sessionService)
        {
            config = options.Value;

            Client = httpClient;

            Client.BaseAddress = new Uri(config.BaseUrl);
            Client.Timeout = TimeSpan.FromSeconds(config.Timeout);

            Client.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/json");

            string? authHeader = sessionService?.Token?.AccessToken;

            if (config.Platform == MongoosePlatform.Cloud)
            {
                authHeader = "Bearer " + authHeader;
            }

            Client.DefaultRequestHeaders.Add("X-Infor-MongooseConfig", config.Configuration);
            Client.DefaultRequestHeaders.Add("X-Infor-MongooseSessionType", "CustomUser");

            Client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderNames.Authorization, authHeader);
        }

        #region Generic
        public async Task<MongooseResponse<BasicResponse>> GetIDOCollection<T>(string? filter, string? props = null, Dictionary<string, string>? parameters = null, bool allIdoProps = false)
        {
            var ido = IDOHelper.GetIDOName<T>();

            if (props == null && !allIdoProps)
            {
                props = Uri.EscapeDataString(IDOHelper.GetIDOProperties<T>());
            }

            if (parameters == null && !string.IsNullOrWhiteSpace(filter))
            {
                parameters = new Dictionary<string, string>()
                {
                    { "filter", filter }
                };
            }

            var output = await GetAsync<BasicResponse>($"json/{ido}{(!allIdoProps ? $"/{props}" : "")}/adv", parameters);

            return output;
        }

        public async Task<T> GetRow<T>(string filter, string? props = null, Dictionary<string, string>? parameters = null, bool allIdoProps = false, bool indexFill = true)
        {
            T output = default;

            var response = await GetIDOCollection<T>(filter, props, parameters, allIdoProps);

            if (response.Success == true && response.Response?.Items?.Count > 0)
            {
                foreach (var item in response.Response.Items)
                {
                    output = IDOHelper.FillProperties<T>(item, indexFill);
                }
            }

            return output;
        }

        public async Task<List<T>> GetRows<T>(string? filter = null, string? props = null, Dictionary<string, string>? parameters = null, bool allIdoProps = false, bool indexFill = true)
        {
            List<T> output = new List<T>();

            var response = await GetIDOCollection<T>(filter, props, parameters, allIdoProps);

            if (response.Success == true && response.Response?.Items?.Count > 0)
            {
                foreach (var item in response.Response.Items)
                {
                    output.Add(IDOHelper.FillProperties<T>(item, indexFill));
                }
            }

            return output;
        }
        #endregion

        #region General
        private async Task<MongooseResponse<TRes>> PostAsync<TReq, TRes>(string action, TReq request, bool isPut = false) where TRes : new()
        {
            MongooseResponse<TRes> output = new MongooseResponse<TRes>();

            try
            {
                HttpContent content = GeneralHelper.GetHttpContent(request);

                string requestStr = await content.ReadAsStringAsync();

                HttpResponseMessage? responsehttp = null;

                if (!isPut)
                {
                    responsehttp = await Client.PostAsync(action, content);
                }
                else
                {
                    responsehttp = await Client.PutAsync(action, content);
                }

                //response.EnsureSuccessStatusCode();

                string responseStr = await responsehttp.Content.ReadAsStringAsync();

                output.StatusCode = responsehttp.StatusCode;

                if (responsehttp.StatusCode == System.Net.HttpStatusCode.OK
                    || responsehttp.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    output.Success = true;

                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Debug("API POST Url: {@Url}{@ActionUrl}", Client.BaseAddress.ToString(), action);

                    // Don't deserialize empty
                    if (responsehttp.StatusCode != System.Net.HttpStatusCode.NoContent)
                    {
                        using Stream responseStream = await responsehttp.Content.ReadAsStreamAsync();
                        using StreamReader sr = new StreamReader(responseStream);
                        using JsonReader reader = new JsonTextReader(sr);

                        output.Response = JsonSerializer.CreateDefault().Deserialize<TRes>(reader);
                    }
                }
                else
                {
                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Information("Unsuccessfull API POST Url: {@Url}{@ActionUrl} HttpCode: {@HttpCode}", Client.BaseAddress.ToString(), action, responsehttp.StatusCode);
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Error(ex, "A web exception occurred calling POST API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An exception occurred calling POST API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }

            return output;
        }

        private async Task<MongooseResponse<TRes>> GetAsync<TRes>(string action, Dictionary<string, string> parameters, bool isDelete = false)
        {
            MongooseResponse<TRes> output = new MongooseResponse<TRes>();

            try
            {
                string requestStr = string.Empty;
                if (parameters?.Count > 0)
                {
                    var lst = parameters.Select(s => $"{s.Key}={Uri.EscapeDataString(s.Value)}").ToList();
                    requestStr = string.Join("&", lst);
                }

                string uri = $"{action}{(!string.IsNullOrEmpty(requestStr) ? "?" + requestStr : "")}";

                //string url = Uri.EscapeUriString(uri);

                HttpResponseMessage? responsehttp = null;

                if (!isDelete)
                {
                    responsehttp = await Client.GetAsync(uri);
                }
                else
                {
                    responsehttp = await Client.DeleteAsync(uri);
                }

                //response.EnsureSuccessStatusCode();

                string responseStr = await responsehttp.Content.ReadAsStringAsync();

                output.StatusCode = responsehttp.StatusCode;

                if (responsehttp.StatusCode == System.Net.HttpStatusCode.OK
                    || responsehttp.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    output.Success = true;

                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Debug("API GET Url: {@Url}{@ActionUrl}", Client.BaseAddress.ToString(), action);

                    // Don't deserialize empty
                    if (responsehttp.StatusCode != System.Net.HttpStatusCode.NoContent)
                    {
                        using (Stream responseStream = await responsehttp.Content.ReadAsStreamAsync())
                        using (StreamReader sr = new StreamReader(responseStream))
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            output.Response = JsonSerializer.CreateDefault().Deserialize<TRes>(reader);
                        }
                    }
                }
                else
                {
                    Log.ForContext("Request", requestStr)
                        .ForContext("Response", responseStr)
                        .Information("Unsuccessfull API GET Url: {@Url}{@ActionUrl} HttpCode: {@HttpCode}", Client.BaseAddress.ToString(), action, responsehttp.StatusCode);
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Error(ex, "A web exception occurred calling GET API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An exception occurred calling GET API Url: {@Url}{@ActionUrl}",
                   Client.BaseAddress.ToString(), action);

                throw ex;
            }

            return output;
        }        
        #endregion
    }
}
