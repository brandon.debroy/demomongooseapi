﻿using DemoMongooseApi.Helpers;
using DemoMongooseApi.Models.IDOs;

namespace DemoMongooseApi.Services
{
    public class DemoService
    {
        private readonly MongooseApiService Service;

        public DemoService(MongooseApiService service)
        {
            Service = service;
        }

        internal async Task<List<AreaInteres>> GetInterestAreas(int company)
        {
            string filter = $"COD_EMPRESA = {company} AND ESTADO = 1";

            List<AreaInteres> output = await Service.GetRows<AreaInteres>(filter);

            return output;
        }

        internal async Task<AreaInteresImagen> GetInterestAreaImage(int company, string area)
        {
            string filter = $"COD_EMPRESA = {company} AND COD_AREA_INTERES = {SqlLiteral.Format(area)}";

            AreaInteresImagen output = await Service.GetRow<AreaInteresImagen>(filter);

            return output;
        }
    }
}
