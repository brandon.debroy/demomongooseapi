﻿using DemoMongooseApi.Models;
using DemoMongooseApi.Models.Mongoose;
using DemoMongooseApi.Services;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Diagnostics;

namespace DemoMongooseApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApiSessionService _sessionService;
        private readonly TokenApiService _tokenApiService;
        private readonly DemoService _demoService;

        public HomeController(ApiSessionService sessionService, TokenApiService tokenService, DemoService demoService)
        {
            _sessionService = sessionService;
            _tokenApiService = tokenService;
            _demoService = demoService;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                if (_sessionService.Token == null)
                {
                    await LoginMongoose();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An error occurred trying to login");

                throw;
            }

            var model = await _demoService.GetInterestAreas(2);

            return View(model);
        }

        public async Task<IActionResult> Image(int company, string id)
        {
            try
            {
                var area = await _demoService.GetInterestAreaImage(company, id);

                if (!string.IsNullOrEmpty(area?.Imagen))
                {
                    return File(Base64FileDecode(area.Imagen), "image/png");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An exception occurred on area image");
            }

            return NotFound();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [NonAction]
        private async Task LoginMongoose()
        {
            Token? token = null;

            var resp = await _tokenApiService.LoginMongoose();

            if (resp?.Success == true && resp?.Token?.Success == true)
            {
                token = new Token
                {
                    AccessToken = resp.Token.Token
                };
            }
            else if (resp?.Success == true && resp.Token == null)
            {
                Log.Error("An error occurred trying to get token: empty successful response");
            }
            else
            {
                Log.Error("An error occurred trying to get token: invalid response: {@Response}", resp);
            }


            if (token != null)
            {
                _sessionService.SetToken(token);

                Log.Information("Access token granted");
            }
            else
            {
                throw new Exception("Invalid token response");
            }
        }

        [NonAction]
        private static byte[] Base64FileDecode(string? base64String)
        {
            if (!string.IsNullOrWhiteSpace(base64String))
            {
                return Convert.FromBase64String(base64String);
            }

            return Array.Empty<byte>();
        }
    }
}