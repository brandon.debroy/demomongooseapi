﻿using DemoMongooseApi.Attributes;
using DemoMongooseApi.Models;
using DemoMongooseApi.Models.Mongoose;
using Serilog;
using System.ComponentModel;
using System.Reflection;

namespace DemoMongooseApi.Helpers
{
    public class IDOHelper
    {
        public static List<EvalProperty> GetEvalProperties(Type type)
        {
            List<EvalProperty> evalProperties = new List<EvalProperty>();

            // Loop through all properties
            PropertyInfo[] propertiesInfo = type.GetProperties();

            foreach (PropertyInfo property in propertiesInfo)
            {
                IDOProperty attr = property.GetCustomAttribute<IDOProperty>();
                IDOIgnore ignoreAttr = property.GetCustomAttribute<IDOIgnore>();

                // Skip if property have ignore attribute
                if (ignoreAttr != null)
                {
                    continue;
                }

                string name = string.IsNullOrWhiteSpace(attr?.Name) ? property.Name : attr.Name.Trim();

                evalProperties.Add(new EvalProperty(property, name));
            }

            return evalProperties;
        }

        public static T FillProperties<T>(List<IDOUpdateProperty> values, bool indexFill)
        {
            Type type = typeof(T);

            List<EvalProperty> properties = GetEvalProperties(type);

            T t = (T)Activator.CreateInstance(type);

            int i = 0;
            int valuesCount = values.Count - 1;

            if (properties.Count > 0 && values?.Count > 1)
            {
                foreach (EvalProperty item in properties)
                {
                    if (item.Property.PropertyType == typeof(string) || !item.Property.PropertyType.IsClass)
                    {
                        try
                        {
                            string? value = indexFill
                                           ? values[i].Value
                                           : values.Where(v => v.Name == item.Name).FirstOrDefault()?.Value;

                            if (!string.IsNullOrEmpty(value))
                            {
                                var converter = TypeDescriptor.GetConverter(item.Property.PropertyType);

                                if (item.Property.PropertyType == typeof(double))
                                    value = value.Replace(".", ",");

                                var result = item.Property.PropertyType == typeof(DateTime) || item.Property.PropertyType == typeof(DateTime?)
                                                ? GeneralHelper.ParseDateTime(value)
                                                : converter.ConvertFrom(value);

                                item.Property.SetValue(t, result);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "An error occurred setting property value");
                        }
                    }

                    i++;
                }
            }

            return t;
        }

        public static string GetIDOName<T>()
        {
            string name = null;

            // Get the class-level attributes.
            IDOClass attr = typeof(T).GetCustomAttribute<IDOClass>();

            if (!string.IsNullOrWhiteSpace(attr?.Name))
            {
                name = attr.Name.Trim();
            }

            return name;
        }

        public static string GetIDOProperties<T>()
        {
            var props = GetEvalProperties(typeof(T));

            return string.Join(",", props.Select(p => p.Name).ToList());
        }
    }
}
