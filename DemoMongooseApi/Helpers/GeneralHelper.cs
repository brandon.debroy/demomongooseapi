﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;

namespace DemoMongooseApi.Helpers
{
    internal static class GeneralHelper
    {
        internal static HttpContent GetHttpContent(object? data, bool jsonBody = true)
        {
            HttpContent content;

            if (jsonBody)
            {
                string payload = JsonConvert.SerializeObject(data);

                content = new StringContent(payload, System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                content = new FormUrlEncodedContent(data as Dictionary<string, string>);
            }

            return content;
        }

        internal static DateTime? ParseDateTime(string value)
        {
            DateTime? dateValue = null;

            if (DateTime.TryParseExact(value, "yyyyMMdd HH:mm:ss.fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsed))
            {
                dateValue = parsed;
            }
            else if (DateTime.TryParseExact(value, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsed2))
            {
                dateValue = parsed2;
            }

            return dateValue;
        }
    }
}