﻿using System;
using System.Globalization;
using System.Text;

namespace DemoMongooseApi.Helpers
{
    /// <summary>
    /// From Mongoose.Core.Common.SqlLiteral (MGShared.dll)
    /// </summary>
    public static class SqlLiteral
    {
        public const string NULL = "NULL";

        public const string UNICODE_LITERAL = "N";

        private const string NON_UNICODE_LITERAL = "";

        public static SqlLiteralType FirstCharStringToSqlLiteralType(string inputValue)
        {
            SqlLiteralType result = SqlLiteralType.Default;
            if (inputValue == null)
            {
                result = SqlLiteralType.Default;
            }
            else if (inputValue.StartsWith(SqlLiteralType.Ansii.ToString().Substring(0, 1), StringComparison.InvariantCultureIgnoreCase))
            {
                result = SqlLiteralType.Ansii;
            }
            else if (inputValue.StartsWith(SqlLiteralType.Smart.ToString().Substring(0, 1), StringComparison.InvariantCultureIgnoreCase))
            {
                result = SqlLiteralType.Smart;
            }
            return result;
        }

        public static string LiteralPrefix(SqlLiteralType characterLiteral, string value)
        {
            string result = "N";
            switch (characterLiteral)
            {
                case SqlLiteralType.Ansii:
                    result = "";
                    break;
                case SqlLiteralType.Default:
                    result = "N";
                    break;
                case SqlLiteralType.Smart:
                    if (value == null)
                    {
                        break;
                    }
                    result = "";
                    foreach (char c in value)
                    {
                        if (c > '\u007f')
                        {
                            result = "N";
                            break;
                        }
                    }
                    break;
            }
            return result;
        }

        public static string Format(object? value)
        {
            return Format(value, "N");
        }

        public static string Format(object? value, string characterLiteral)
        {
            if (value == null || value == DBNull.Value)
            {
                return "NULL";
            }
            return Format(TextUtil.FormatNormalizedString(value), value.GetType(), characterLiteral);
        }

        public static string Format<T>(T? value) where T : struct
        {
            if (value.HasValue)
            {
                return Format(value.Value);
            }
            return Format(null);
        }

        public static string Format(string literal, SqlLiteralFormatFlags options)
        {
            return Format(literal, options, "N");
        }

        public static string Format(string literal, SqlLiteralFormatFlags options, string characterLiteral)
        {
            if ((options & SqlLiteralFormatFlags.UseQuotes) == SqlLiteralFormatFlags.UseQuotes)
            {
                string arg = string.Empty;
                if ((options & SqlLiteralFormatFlags.AnsiString) != SqlLiteralFormatFlags.AnsiString)
                {
                    arg = characterLiteral;
                }
                return string.Format("{0}'{1}'", arg, literal.Replace("'", "''"));
            }
            return literal;
        }

        public static string Format(string literal, Type valueType)
        {
            return Format(literal, valueType, "N");
        }

        public static string Format(string literal, Type valueType, string characterLiteral)
        {
            SqlLiteralFormatFlags sqlLiteralFormatFlags = SqlLiteralFormatFlags.None;
            if (valueType == typeof(string) || valueType == typeof(DateTime) || valueType == typeof(Guid))
            {
                sqlLiteralFormatFlags |= SqlLiteralFormatFlags.UseQuotes;
            }
            return Format(literal, sqlLiteralFormatFlags, characterLiteral);
        }
    }

    /// <summary>
    /// From Mongoose.Core.Common.TextUtil (MGShared.dll)
    /// </summary>
    internal static class TextUtil
    {
        public static string FormatBinaryString(byte[] buffer)
        {
            if (buffer == null)
            {
                return string.Empty;
            }
            return Convert.ToBase64String(buffer, Base64FormattingOptions.InsertLineBreaks);
        }

        public static string FormatNormalizedDate(DateTime date, DateFormatOptions Options)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string text = (((Options & DateFormatOptions.ClientInternal) == 0) ? string.Empty : "/");
            stringBuilder.AppendFormat("{0:0000}{3}{1:00}{3}{2:00}", date.Year, date.Month, date.Day, text);
            if ((Options & DateFormatOptions.DateOnly) == 0)
            {
                stringBuilder.AppendFormat(" {0:00}:{1:00}:{2:00}", date.Hour, date.Minute, date.Second);
                if ((Options & DateFormatOptions.OmitMilliseconds) == 0)
                {
                    stringBuilder.AppendFormat(".{0:000}", date.Millisecond);
                }
            }
            return stringBuilder.ToString();
        }


        public static string FormatNormalizedDate(DateTime date)
        {
            return FormatNormalizedDate(date, DateFormatOptions.Default);
        }

        public static string FormatNormalizedTimeSpan(TimeSpan duration)
        {
            return duration.ToString("c");
        }

        public static string FormatNormalizedInt16(short shortValue)
        {
            return shortValue.ToString("D", CultureInfo.InvariantCulture);
        }


        public static string FormatNormalizedInt32(int intValue)
        {
            return intValue.ToString("D", CultureInfo.InvariantCulture);
        }


        public static string FormatNormalizedString(object? objValue)
        {
            string? result = null;
            
            if (objValue != null && objValue != DBNull.Value)
            {
                Type type = objValue.GetType();
                if (Nullable.GetUnderlyingType(type) != null)
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                if (type == typeof(string))
                {
                    result = objValue.ToString()?.Replace("'", "''"); // Added single quote replacement
                }
                else if (objValue == DBNull.Value)
                {
                    result = string.Empty;
                }
                else if (type == typeof(char))
                {
                    result = ((char)objValue).ToString();
                }
                else if (type == typeof(DateTime))
                {
                    result = FormatNormalizedDate((DateTime)objValue);
                }
                else if (type == typeof(TimeSpan))
                {
                    result = FormatNormalizedTimeSpan((TimeSpan)objValue);
                }
                else if (type == typeof(int))
                {
                    result = FormatNormalizedInt32((int)objValue);
                }
                else if (type == typeof(uint))
                {
                    result = ((uint)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(short))
                {
                    result = FormatNormalizedInt16((short)objValue);
                }
                else if (type == typeof(ushort))
                {
                    result = ((ushort)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(long))
                {
                    result = ((long)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(ulong))
                {
                    result = ((ulong)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(sbyte))
                {
                    result = ((sbyte)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(byte))
                {
                    result = ((byte)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(float))
                {
                    result = ((float)objValue).ToString(CultureInfo.InvariantCulture);
                }
                else if (type == typeof(double))
                {
                    result = ((double)objValue).ToString(CultureInfo.InvariantCulture);
                }
                else if (type == typeof(Guid))
                {
                    result = ((Guid)objValue).ToString("D", CultureInfo.InvariantCulture);
                }
                else if (type == typeof(decimal))
                {
                    result = ((decimal)objValue).ToString(CultureInfo.InvariantCulture);
                }
                else if (type == typeof(bool))
                {
                    result = ((bool)objValue).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    if (!(type == typeof(byte[])))
                    {
                        throw new ArgumentException($"TextUtil.FormatNormalizedString: unsupported data type {objValue.GetType().Name}");
                    }
                    result = FormatBinaryString((byte[])objValue);
                }
            }
            
            return result ?? string.Empty;
        }
    }

    public enum SqlLiteralType
    {
        Default,
        Ansii,
        Smart
    }

    [Flags]
    public enum SqlLiteralFormatFlags
    {
        None = 0,
        UseQuotes = 1,
        AnsiString = 2
    }

    [Flags]
    public enum DateFormatOptions
    {
        Default = 0,
        DateOnly = 1,
        OmitMilliseconds = 2,
        ClientInternal = 4
    }
}
