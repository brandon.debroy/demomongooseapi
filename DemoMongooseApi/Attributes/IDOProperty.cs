﻿using System;

namespace DemoMongooseApi.Attributes
{
    /// <summary>
    /// Map a property with an IDO Property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class IDOProperty : Attribute
    {
        /// <summary>
        /// Name of the record
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Creates new attribute with the property name as the IDO Property name
        /// </summary>
        public IDOProperty() { }

        /// <summary>
        /// Creates a new attribute
        /// </summary>
        /// <param name="name">Name of the IDO Property</param>
        public IDOProperty(string name)
        {
            this.Name = name;
        }
    }
}