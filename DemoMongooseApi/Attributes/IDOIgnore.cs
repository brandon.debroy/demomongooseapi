﻿namespace DemoMongooseApi.Attributes
{
    /// <summary>
    /// Marks a property as not used
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class IDOIgnore : Attribute
    {
        /// <summary>
        /// Creates a new ignore attribute
        /// </summary>
        public IDOIgnore()
        {
        }
    }
}