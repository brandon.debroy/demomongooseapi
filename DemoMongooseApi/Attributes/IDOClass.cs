﻿using System;

namespace DemoMongooseApi.Attributes
{
    /// <summary>
    /// Map a class with an IDO name
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class IDOClass : Attribute
    {
        /// <summary>
        /// Name of the record
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Creates a new attribute
        /// </summary>
        /// <param name="name">Name of the IDO</param>
        public IDOClass(string name)
        {
            this.Name = name;
        }
    }
}