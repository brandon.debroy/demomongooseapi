using DemoMongooseApi.Models;
using DemoMongooseApi.Models.Mongoose;
using DemoMongooseApi.Services;
using Microsoft.Extensions.Configuration;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateBootstrapLogger();

Log.Information("Starting up");

try
{
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((ctx, lc) => lc.ReadFrom.Configuration(ctx.Configuration));

    // Add services to the container.
    builder.Services.AddControllersWithViews();

    var configSection = builder.Configuration.GetSection(MongooseRESTOptions.ConfigKey);

    var bypassHttps = configSection.GetValue(nameof(MongooseRESTOptions.BypassHttps), false);
    var enableTokenRefresh = configSection.GetValue(nameof(MongooseRESTOptions.EnableTokenRefresh), false);

    builder.Services.AddSingleton<ApiSessionService>();
    builder.Services.Configure<MongooseRESTOptions>(configSection);

    builder.Services.AddHttpClient<MongooseApiService>()
        .ConfigurePrimaryHttpMessageHandler(() =>
        {
            var handler = new HttpClientHandler();

            if (bypassHttps)
            {
                handler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => {
                    return true;
                };
            }

            return handler;
        });

    builder.Services.AddHttpClient<TokenApiService>()
        .ConfigurePrimaryHttpMessageHandler(() =>
        {
            var handler = new HttpClientHandler();

            if (bypassHttps)
            {
                handler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => {
                    return true;
                };
            }

            return handler;
        });

    builder.Services.AddTransient<DemoService>();

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();

    app.UseRouting();

    app.UseAuthorization();

    app.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled general exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}
